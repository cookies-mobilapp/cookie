SütiNeked

Egy receptes appot szeretnénk készíteni, amiben nemcsak recept kategóriák alapján lehet keresni, hanem az alapján is, hogy mi van éppen "otthon". Szeretnénk megoldani azt is, hogy ha a felhsználó azt szeretné, kizárólag azokat a hozzávalókat (és maximum abban a mennyiségben) tartalmazó recepteket adjon vissza az app, amiket a keresése során megadott, de ha azt szeretné, akkor egyéb recepteket is visszaadna, olyanokat amelyek más alapanyagokat is tartalmazhatnak.

Megvalósítandó elemek:

	-keresés
	
		-konkrét receptre keresni
		
		-hozzávalók alapján keresni, választani a keresés szűrését
		
			-adott hozzávalók biztos benne legyenek	//ezeket mondjuk jelölővel lehetne állítani
			
			-csak az adott hozzávalók legyenek benne	//ezeket mondjuk jelölővel lehetne állítani
			
			-mennyiség beállítás, hogy adott mennyiségnél ne legyen több
			
			-adott hozzávalók ne legyenek benne, pl érzékenyeknek jó
			
		-kategóriák szerint szűkítés, pl: kelttészták stb. //lenyíló
		
		-elkészítési idő szerint szűkíteni a találatokat
		
		-recept beküldő szerint keresni
		
	-találatokat, rendezni lehessen (alapból a kereséssel legmegegyezőbb van elöl)
	
		-abc sorrend (bár nem tudom ez mire jó, de lehet valaki így szeretné)
		
		-értékelés szerint
		
	-kinyomtatási/ offline lementési lehetőség
	
	-képek, videók (az elkészítésről)
	
	*egy felhasználói bejelentkezési lehetőség, ahonnan akár maga a felhasználó is tud receptet feltölteni vagy praktikákat megosztani?
	
Csapattagok:

	Arany Enikő, Böndicz Anna
	
Csapatnév:

	Süti