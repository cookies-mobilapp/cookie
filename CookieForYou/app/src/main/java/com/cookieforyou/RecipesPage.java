package com.cookieforyou;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cookieforyou.model.Recipe;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.cookieforyou.SearchPage.list;
import static com.cookieforyou.SearchPage.searchText;
import static com.cookieforyou.StartPage.selectedItem;

public class RecipesPage extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    List<Recipe> recipes = new ArrayList<>();
    ListViewAdapter adapter;
    List<Recipe> arraylist = new ArrayList<>();
    BottomNavigationView bottomNavigation;
    ListView listView;

    // Booleans for time checkboxes on settingsPage
    private boolean oneHourOnOff = true;
    private boolean twoHourOnOff = true;
    private boolean threeHourOnOff = true;
    private boolean otherHoursOnOff = true;

    // Booleans for difficulty checkboxes on settingsPage
    private boolean easyOnOff = true;
    private boolean midOnOff = true;
    private boolean hardOnOff = true;

    // Booleans for rating checkboxes on settingsPage
    private boolean threeOnOff = true;
    private boolean fourOnOff = true;
    private boolean fiveOnOff = true;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipes_page);

        bottomNavigation = findViewById(R.id.bottom_navigation);        //Alsó navigációs menü
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        list = (ListView) findViewById(R.id.RecipeList);
        list.setOnItemClickListener(onItemClickListener);    //lista elemre kattintásfigyelő

        setupSharedPreferences();

        foundedItems();

        ////////////////////////////////////////LEGÖRDÜLŐ LISTA KEZDETE/////////////////////////////

        //Lenyíló ista view
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        //Lenyíló lista elemei
        List<String> categories = new ArrayList<String>();
        categories.add("Alapértelmezett");
        categories.add("ABC szerint növekvő");
        categories.add("ABC szerint csökkenő");
        categories.add("Elkészítési idő szerint növekvő");
        categories.add("Elkészítési idő szerint csökkenő");
        categories.add("Értékelés szerint növekvő");
        categories.add("Értékelés szerint csökkenő");

        //Adapter a lenyíló listához
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        //Legördülő stílus elrendezés - lista nézet választógombbal
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // adapter csatlakoztatása a sipnnerhez
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(selectedListener);

        ////////////////////////////////////////LEGÖRDÜLŐ LISTA VÉGE////////////////////////////////

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SettingsActivity.class); //open the filter page
                startActivity(intent);
            }
        });
    }

    private void foundedItems() {
        ////////////////////////////////////////TALÁLT RECEPT LISTA KEZDEZTE////////////////////////
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);   //Adatbázis beállítás
        databaseAccess.openDatabase();

        recipes = databaseAccess.getRecipes();                  //Receptek lekérése adatbázisból
        databaseAccess.closeDatabase();

        list = (ListView) findViewById(R.id.RecipeList);        //Lista view megtalálása

        arraylist.addAll(recipes);                              //receptek lementése egy ArrayList-be

        adapter = new ListViewAdapter(this, recipes);   //Adapter beállítása a receptekhez
        adapter.filter(searchText);                             //Lista szűrése, a keresés alapján
        searchText = searchText.toLowerCase(Locale.getDefault());
        recipes.clear();

        if (searchText.length() == 0) {                     //Ha nem írtunk be semmit a keresésbe,
            recipes.addAll(arraylist);                      //visszadja az összes receptet
        } else {
            for (Recipe wp : arraylist) {  //különben a keresésnek megfelelően adja vissza a recepteket
                if (wp.getRecipeName().toLowerCase(Locale.getDefault()).contains(searchText)) {
                    Boolean addItem = true;
                    //:TODO
                    //filter of difficulty easyOnOff, midOnOff, hardOnOff -> there's no such data in the database

                    //filter of rating -> threeOnOff, fourOnOff, fiveOnOff
                    if (addItem && addItem) {
                        if (threeOnOff) {
                            if (wp.getRecipeRating() < 2.0 || wp.getRecipeRating() > 3.0) addItem = false;
                        }
                        if ((fourOnOff && !threeOnOff) || (fourOnOff && !addItem)) {
                            if (wp.getRecipeRating() < 3.0 || wp.getRecipeRating() > 4.0) addItem = false;
                            else addItem = true;
                        }
                        if ( (fiveOnOff && !threeOnOff && !fourOnOff) || (fiveOnOff && !addItem)) {
                            if (wp.getRecipeRating() != 5.0) addItem = false;
                            else addItem = true;
                        }
                    }

                    //filter of time -> oneHourOnOff, twoHourOnOff, threeHourOnOff, otherHoursOnOff
                    if (addItem) {
                        if (oneHourOnOff) {
                            if (Integer.parseInt(wp.getRecipeTime()) > 60) addItem = false;
                            else addItem = true;
                        }
                        if ((twoHourOnOff && !oneHourOnOff) || (twoHourOnOff && !addItem)) {
                            if (Integer.parseInt(wp.getRecipeTime()) < 60 || Integer.parseInt(wp.getRecipeTime()) > 120) addItem = false;
                            else addItem = true;
                        }
                        if ((threeHourOnOff && !oneHourOnOff && !twoHourOnOff) || (threeHourOnOff && !addItem)) {
                            if (Integer.parseInt(wp.getRecipeTime()) < 120 || Integer.parseInt(wp.getRecipeTime()) > 180) addItem = false;
                            else addItem = true;
                        }
                        if ((otherHoursOnOff && !oneHourOnOff && !twoHourOnOff && !threeHourOnOff) || (otherHoursOnOff && !addItem)) {
                            if (Integer.parseInt(wp.getRecipeTime()) < 180) addItem = false;
                            else addItem = true;
                        }
                    }
                    if (addItem) recipes.add(wp);
                }
            }
        }
        if(recipes.isEmpty())           //Ha nincs találat
        {
            TextView emptyText = findViewById(R.id.EmptyText);
            emptyText.setText("Nincs találat");      //Kiírja hogy nincs találat
        }

        //Adapter beállítása
        adapter.notifyDataSetChanged();
        list.setAdapter(adapter);
        arraylist.clear();
        ////////////////////////////////////////TALÁLT RECEPT LISTA VÉGE////////////////////////////
    }


    //Alsó menu navigáció
    final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {                                             //4 menű választása
                        case R.id.start:
                            Intent intent = new Intent(getBaseContext(), StartPage.class);  //kezdőoldal megnyitása
                            startActivity(intent);
                            return true;
                        case R.id.saved:
                            notWork("Ez a funkció  még nem elérhető");                  //mentett receptek (nem működik)
                            return true;
                        case R.id.search:
                            Intent intent2 = new Intent(getBaseContext(), SearchPage.class);    //Keresés oldal megnyitása
                            startActivity(intent2);
                            return true;
                        case R.id.profil:
                            notWork("Ez a funkció  még nem elérhető");                  //profil (nem működik)
                            return true;
                    }
                    return false;
                }
            };

    //Lista elemre kattintás
       final ListView.OnItemClickListener onItemClickListener =
                new ListView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {      //Megnyitja a OneRecipePaget
                        Intent intent = new Intent(getBaseContext(), OneRecipePage.class);
                        //intent.putExtra("itemId", id + ""); //+ "" to make it string, of course you can use long but you have to remeber to use intent.getLongExtra(...) in details Activity
                        selectedItem = list.getItemIdAtPosition(position)+1;  //lementi a kiválasztott listaelem pozícióját, a OneRecpePage számára
                        intent.putExtra("selectedItem",selectedItem);
                        startActivity(intent);
                    }
                };

    //Lenyíló lista elemre kattintás
    final AdapterView.OnItemSelectedListener selectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> spinner, View container, int position, long id) {
            if (position > 0) {
                notWork("Ez a funkció még nem elérhető");
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    };

    //Nem működő funkciókra
    public void notWork(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    private void setupSharedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        oneHourOnOff = sharedPreferences.getBoolean("one_hour",true);
        twoHourOnOff = sharedPreferences.getBoolean("two_hour",true);
        threeHourOnOff = sharedPreferences.getBoolean("three_hour",true);
        otherHoursOnOff = sharedPreferences.getBoolean("other_hours",true);

        easyOnOff = sharedPreferences.getBoolean("easy",true);
        midOnOff = sharedPreferences.getBoolean("mid",true);
        hardOnOff = sharedPreferences.getBoolean("hard",true);

        threeOnOff = sharedPreferences.getBoolean("three",true);
        fourOnOff = sharedPreferences.getBoolean("four",true);
        fiveOnOff = sharedPreferences.getBoolean("five",true);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    //if something changes on the SettingsPage, the belonging values must change, too
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("one_hour")) oneHourOnOff = sharedPreferences.getBoolean("one_hour",true);
        if (key.equals("two_hour")) twoHourOnOff = sharedPreferences.getBoolean("two_hour", true);
        if (key.equals("three_hour")) threeHourOnOff = sharedPreferences.getBoolean("three_hour", true);
        if (key.equals("other_hours")) otherHoursOnOff = sharedPreferences.getBoolean("other_hours",true);

        if (key.equals("easy")) easyOnOff = sharedPreferences.getBoolean("easy",true);
        if (key.equals("mid")) midOnOff = sharedPreferences.getBoolean("mid",true);
        if (key.equals("hard")) hardOnOff = sharedPreferences.getBoolean("hard",true);

        if (key.equals("three")) threeOnOff = sharedPreferences.getBoolean("three",true);
        if (key.equals("four")) fourOnOff = sharedPreferences.getBoolean("four",true);
        if (key.equals("five")) fiveOnOff = sharedPreferences.getBoolean("five",true);

        recipes.clear();
        foundedItems();
    }
}
