package com.cookieforyou;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cookieforyou.model.Category;
import com.cookieforyou.model.Comment;
import com.cookieforyou.model.Ingredient;
import com.cookieforyou.model.Recipe;
import com.cookieforyou.model.RecipeCategory;
import com.cookieforyou.model.RecipeIngredient;
import com.cookieforyou.model.SavedRecipe;
import com.cookieforyou.model.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
//Még nem működik...
public class OneRecipePage extends AppCompatActivity {

    // Lists for data
    private List<Category> categories = new ArrayList<>();
    private List<Comment> comments = new ArrayList<>();
    private List<Ingredient> ingredients = new ArrayList<>();
    private List<Recipe> recipes = new ArrayList<>();
    private List<RecipeCategory> recipesCategories = new ArrayList<>();
    private List<RecipeIngredient> recipesIngredients = new ArrayList<>();
    private List<SavedRecipe> savedRecipes = new ArrayList<>();
    private List<User> users = new ArrayList<>();

    private Long selectedItem;

    BottomNavigationView bottomNavigation;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.one_recipe_page);

        //using the database and create lists of data from the database
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.openDatabase();

        categories = databaseAccess.getCategories();
        comments = databaseAccess.getComments();
        ingredients = databaseAccess.getIngredients();
        recipes = databaseAccess.getRecipes();
        recipesCategories = databaseAccess.getRecipeCategories();
        recipesIngredients = databaseAccess.getRecipeIngredients();
        savedRecipes = databaseAccess.getSavedRecipes();
        users = databaseAccess.getUsers();

        databaseAccess.closeDatabase();

        //get the id of the selectedItem
        selectedItem = getIntent().getLongExtra("selectedItem",0L);
        Recipe recipe = recipes.get(Math.toIntExact(selectedItem));

        //add data to views
        TextView view = findViewById(R.id.OneRecipeName);
        view.setText(recipe.getRecipeName());

        view = findViewById(R.id.RecipeOwner);
        User user = users.get(recipe.getUserId());
        view.setText("Beküldő: "+user.getUserName());

        view = findViewById(R.id.RecipeTime);
        view.setText("Elkészítési idő: "+recipe.getRecipeTime());

        view = findViewById(R.id.RecipeRating);
        view.setText("Értékelés: "+recipe.getRecipeRating());

        ImageView imageView = findViewById(R.id.BigRecipeImage);

        //create the bottomNavigation
        bottomNavigation = findViewById(R.id.bottom_navigation);    //alsó menü navigáció view beállítása
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
    }

    //Alsó menu navigáció
    final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {                                             //4 menű választása
                        case R.id.start:
                            Intent intent = new Intent(getBaseContext(), StartPage.class);  //kezdőoldal megnyitása
                            startActivity(intent);
                            return true;
                        case R.id.saved:
                            notWork("Ez a funkció még nem elérhető");                  //mentett receptek (nem működik)
                            return true;
                        case R.id.search:
                            Intent intent2 = new Intent(getBaseContext(), SearchPage.class);    //Keresés oldal megnyitása
                            startActivity(intent2);
                            return true;
                        case R.id.profil:
                            notWork("Ez a funkció még nem elérhető");                  //profil (nem működik)
                            return true;
                    }
                    return false;
                }
            };

    //nem működő funkciókra, toast
    public void notWork(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
