package com.cookieforyou.model;

public class RecipeIngredient {
    private Integer recipeId;
    private Integer ingredientId;
    private Double quantityIngredient;
    private Integer unitId;

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Double getQuantityIngredient() {
        return quantityIngredient;
    }

    public void setQuantityIngredient(Double quantityIngredient) {
        this.quantityIngredient = quantityIngredient;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }
}
