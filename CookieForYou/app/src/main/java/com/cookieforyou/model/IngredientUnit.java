package com.cookieforyou.model;

public class IngredientUnit {

    private Integer ingredientUnitId;
    private String ingredientUnitName;

    public Integer getIngredientUnitId() {
        return ingredientUnitId;
    }

    public void setIngredientUnitId(Integer ingredientUnitId) {
        this.ingredientUnitId = ingredientUnitId;
    }

    public String getIngredientUnit() {
        return ingredientUnitName;
    }

    public void setIngredientUnit(String ingredientUnit) {
        this.ingredientUnitName = ingredientUnit;
    }
}
