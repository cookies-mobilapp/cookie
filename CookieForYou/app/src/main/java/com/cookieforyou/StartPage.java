package com.cookieforyou;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.cookieforyou.model.Recipe;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.cookieforyou.R.id.FirstRecipe;
import static com.cookieforyou.R.id.add;

public class StartPage extends AppCompatActivity {

    private List<String> recipeNames = new ArrayList<>();
    private List<Integer> recipeImages = new ArrayList<>();
    List<Recipe> recipes;

    BottomNavigationView bottomNavigation;

    @SuppressLint("StaticFieldLeak")
    public static ListView listView;
    public static Long selectedItem;
    @SuppressLint("StaticFieldLeak")
    public static LinearLayout linearLayout;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_page);                        //layout beállítása
        bottomNavigation = findViewById(R.id.bottom_navigation);   //alsó menü navigáció view beállítása

        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener); //alsó menü navigáció kattintás figyelő

        listView = findViewById(R.id.Recipes);                      //Recept lista view
        linearLayout = findViewById(R.id.linearLayout);             //kezdő recept view

        listView.setOnItemClickListener(onItemClickListener);
        linearLayout.setOnClickListener(onClickListener);

        //Adatbázis beállítás
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.openDatabase();

        // Adatok
        recipes = databaseAccess.getRecipes();                   //receptek lehívás az adatbázisból
        databaseAccess.closeDatabase();

        String firstRecipe = recipes.get(0).getRecipeName();    //első recept neve

        TextView textView = findViewById(FirstRecipe);         //első recept neve view
        textView.setText(firstRecipe);

        ////////////////////////////////////////LEGÖRDÜLŐ LISTA KEZDETE/////////////////////////////
        //spinner layout
        Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner lenyíló lista elemei
        List<String> sort = new ArrayList<String>();
        sort.add("Alapértelmezett");
        sort.add("ABC szerint növekvő");
        sort.add("ABC szerint csökkenő");
        sort.add("Elkészítési idő szerint növekvő");
        sort.add("Elkészítési idő szerint csökkenő");
        sort.add("Értékelés szerint növekvő");
        sort.add("Értékelés szerint csökkenő");

        // Adapter a spinnerhez
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sort);

        //Legördülő stílus elrendezés - lista nézet választógombbal
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // adapter csatlakoztatása a sipnnerhez
        spinner.setAdapter(dataAdapter);

        spinner.setOnItemSelectedListener(selectedListener);

        ////////////////////////////////////////LEGÖRDÜLŐ LISTA VÉGE////////////////////////////////

        ////////////////////////////////////////RECEPT LISTA KEZDEZTE///////////////////////////////

        int[] image = new int[] {R.drawable.piskota_tekercs, R.drawable.csokis_keksz, R.drawable.kokusz_golyo,
                R.drawable.palacsinta, R.drawable.sos_rud, R.drawable.meggyes_kevert};      //képek
        for(int i = 0; i < recipes.size(); i++)     //receptek Nevének és Képnének lementése
        {
            recipeNames.add(recipes.get(i).getRecipeName());
            recipeImages.add(image[recipes.get(i).getRecipeId()-1]);
        }

        List<HashMap<String,String>> aList = new ArrayList<HashMap<String, String>>();
        for (int i = 1; i < recipes.size(); i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("ListTitle", recipeNames.get(i));
            hm.put("ListImages",Integer.toString(recipeImages.get(i)));
            aList.add(hm);
        }

        String[] from = {
                "ListImages","ListTitle"
        };
        int[] to = {
                R.id.RecipeImage, R.id.RecipeName
        };

        SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(),aList, R.layout.list_view_items,from,to);
        @SuppressLint("CutPasteId") ListView simpleListview = (ListView)findViewById(R.id.Recipes);
        simpleListview.setAdapter(simpleAdapter);

        ////////////////////////////////////////RECEPT LISTA VÉGE///////////////////////////////////
    }

    //Új oldal nyitás
    public void openPage(Class activity) {
        Intent intent = new Intent(getBaseContext(), activity);
        startActivity(intent);
    }

    //Alsó menu navigáció
    final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {                                             //4 menű választása
                        case R.id.start:
                            Intent intent = new Intent(getBaseContext(), StartPage.class);  //kezdőoldal megnyitása
                            startActivity(intent);
                            return true;
                        case R.id.saved:
                            notWork("Ez a funkció még nem elérhető");                  //mentett receptek (nem működik)
                            return true;
                        case R.id.search:
                            Intent intent2 = new Intent(getBaseContext(), SearchPage.class);    //Keresés oldal megnyitása
                            startActivity(intent2);
                            return true;
                        case R.id.profil:
                            notWork("Ez a funkció még nem elérhető");                  //profil (nem működik)
                            return true;
                    }
                    return false;
                }
            };

    //Lista elemre kattintás
    final ListView.OnItemClickListener onItemClickListener =
            new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {      //Megnyitja a OneRecipeOldalt
                    Intent intent = new Intent(getBaseContext(), OneRecipePage.class);
                    //intent.putExtra("itemId", id + ""); //+ "" to make it string, of course you can use long but you have to remeber to use intent.getLongExtra(...) in details Activity
                    selectedItem = listView.getItemIdAtPosition(position)+1;     //lementi a kiválasztott listaelem pozícióját, a OneRecpePage számára
                    intent.putExtra("selectedItem",selectedItem);
                    startActivity(intent);
                    }
            };

    //Kezdő receptre kattintás
    final LinearLayout.OnClickListener onClickListener =
            new LinearLayout.OnClickListener() {
                @Override
                public void onClick(View v) {                                      //megnyitja a OnRecpiePage oldalt
                    Intent intent = new Intent(getBaseContext(), OneRecipePage.class);
                    selectedItem = recipes.get(0).getRecipeId().longValue()-1;      //lementi a kiválasztott recept id-jét, a OneRecpePage számára
                    intent.putExtra("selectedItem",selectedItem);
                    startActivity(intent);
                    }
            };

    //Lenyíló lista elemre kattintás
    final AdapterView.OnItemSelectedListener selectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> spinner, View container, int position, long id) {
            if(position > 0){
                notWork("Ez a funkció még nem elérhető");
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }
    };

    //nem működő funkciókra, toast
    public void notWork(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
