package com.cookieforyou;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cookieforyou.model.Recipe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Recipe> recipeList = null;
    private ArrayList<Recipe> arraylist;
    private int images;

    //Listanézet adaptere
    ListViewAdapter(Context context, List<Recipe> recipeList) {
        this.recipeList = recipeList;
        inflater = LayoutInflater.from(context);
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(recipeList);
    }

    public static class ViewHolder {
        TextView name;
        ImageView image;
    }

    //lista elemek száma
    @Override
    public int getCount() {
        return recipeList.size();
    }

    //lista elemek, pozíció alapján
    @Override
    public Recipe getItem(int position) {
        return recipeList.get(position);
    }

    //lista elemek pozíció id-ja
    @Override
    public long getItemId(int position) {
        return position;
    }

    //Listanézet
    @SuppressLint("InflateParams")
    public View getView(final int position, View view, ViewGroup parent) {
        int[] images = new int[] {R.drawable.piskota_tekercs, R.drawable.csokis_keksz, R.drawable.kokusz_golyo,
                R.drawable.palacsinta, R.drawable.sos_rud, R.drawable.meggyes_kevert};
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.list_view, null);      //lista nézet beállítása
            //name és image nézetek megtalálása
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.image = (ImageView) view.findViewById(R.id.image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //Az eredmény beállítása, a name és image nézetekhez
        holder.name.setText(recipeList.get(position).getRecipeName());
        holder.image.setImageResource(images[recipeList.get(position).getRecipeId()-1]);
        return view;
    }

    //Szűrés
    void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        recipeList.clear();
        if (charText.length() == 0) {
            recipeList.addAll(arraylist);     //Ha nem írtunk be semmit, visszaadja az összes elemet
        } else {
            for (Recipe wp : arraylist) {     //különben a keresésnek megfelelően adja vissza a recepteket
                if (wp.getRecipeName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    recipeList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
