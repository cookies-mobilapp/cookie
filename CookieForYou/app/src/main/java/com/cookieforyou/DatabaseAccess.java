package com.cookieforyou;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cookieforyou.model.Category;
import com.cookieforyou.model.Comment;
import com.cookieforyou.model.Ingredient;
import com.cookieforyou.model.IngredientUnit;
import com.cookieforyou.model.Recipe;
import com.cookieforyou.model.RecipeCategory;
import com.cookieforyou.model.RecipeIngredient;
import com.cookieforyou.model.SavedRecipe;
import com.cookieforyou.model.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess {
    private SQLiteOpenHelper sqLiteOpenHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static DatabaseAccess databaseAccess;

    /**
     * Private constructor to avoid object creation from outside classes.
     *
     * @param context the Context
     */
    private DatabaseAccess(Context context) {
        this.sqLiteOpenHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (databaseAccess == null) {
            databaseAccess = new DatabaseAccess(context);
        }
        return databaseAccess;
    }

    /**
     * Open the database connection.
     */
    public void openDatabase() {
        this.sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void closeDatabase() {
        if (sqLiteDatabase != null) {
            this.sqLiteDatabase.close();
        }
    }

    /**
     * Read from the database.
     *
     * @return a List of Categories
     */
    public List<Category> getCategories() {
        List<Category> list = new ArrayList<>();
        //set the cursor
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM categories", null);
        cursor.moveToFirst();
        //load the data into a list
        while (!cursor.isAfterLast()) {
            Category category = new Category();

            category.setCategoryId(cursor.getInt(0));
            category.setCategoryName(cursor.getString(1));
            category.setCategoryGroupId(cursor.getInt(2));

            list.add(category);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of Comments
     */
    public List<Comment> getComments() {
        List<Comment> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM comments", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Comment comment = new Comment();

            comment.setCommentId(cursor.getInt(0));
            comment.setCommentText(cursor.getString(1));
            comment.setRecipeId(cursor.getInt(2));
            comment.setUserId(cursor.getInt(3));

            list.add(comment);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of Ingredients
     */
    public List<Ingredient> getIngredients() {
        List<Ingredient> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM ingredients", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Ingredient ingredient = new Ingredient();

            ingredient.setIngredientId(cursor.getInt(0));
            ingredient.setIngredientName(cursor.getString(1));

            list.add(ingredient);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of IngredientUnits
     */
    public List<IngredientUnit> getIngredientUnits() {
        List<IngredientUnit> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM ing_unit", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            IngredientUnit ingredientUnit = new IngredientUnit();

            ingredientUnit.setIngredientUnitId(cursor.getInt(0));
            ingredientUnit.setIngredientUnit(cursor.getString(1));

            list.add(ingredientUnit);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of Recipes
     */
    public List<Recipe> getRecipes() {
        List<Recipe> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM recipes", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Recipe recipe = new Recipe();

            recipe.setRecipeId(cursor.getInt(0));
            recipe.setRecipeName(cursor.getString(1));
            recipe.setRecipeDescription(cursor.getString(2));
            recipe.setRecipeRating(cursor.getDouble(3));
            recipe.setUserId(cursor.getInt(4));
            recipe.setRecipeTime(cursor.getString(5));
            recipe.setRecipeImage(cursor.getString(6));
            recipe.setRecipeVideo(cursor.getString(7));

            list.add(recipe);
            cursor.moveToNext();
        }


        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of RecipeCategories
     */
    public List<RecipeCategory> getRecipeCategories() {
        List<RecipeCategory> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM rec_cat", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            RecipeCategory recipeCategory = new RecipeCategory();

            recipeCategory.setRecipeId(cursor.getInt(0));
            recipeCategory.setCategoryId(cursor.getInt(1));

            list.add(recipeCategory);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of RecipeIngredients
     */
    public List<RecipeIngredient> getRecipeIngredients() {
        List<RecipeIngredient> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM rec_ing", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            RecipeIngredient recipeIngredient = new RecipeIngredient();

            recipeIngredient.setRecipeId(cursor.getInt(0));
            recipeIngredient.setIngredientId(cursor.getInt(1));
            recipeIngredient.setQuantityIngredient(cursor.getDouble(2));
            recipeIngredient.setUnitId(cursor.getInt(3));

            list.add(recipeIngredient);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of SavedRecipes
     */
    public List<SavedRecipe> getSavedRecipes() {
        List<SavedRecipe> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM saved_rec", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            SavedRecipe savedRecipe = new SavedRecipe();

            savedRecipe.setRecipeId(cursor.getInt(0));
            savedRecipe.setUserId(cursor.getInt(1));

            list.add(savedRecipe);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }

    /**
     * Read from the database.
     *
     * @return a List of Users
     */
    public List<User> getUsers() {
        List<User> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM saved_rec", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = new User();

            user.setUserId(cursor.getInt(0));
            user.setUserName(cursor.getString(1));
            user.setUserEmail(cursor.getString(2));
            user.setUserPassword(cursor.getString(3));
            user.setUserSex(cursor.getString(4));
            user.setUserBirthDate(cursor.getString(5));
            user.setUserExperience(cursor.getString(6));
            user.setUserImage(cursor.getString(7));

            list.add(user);
            cursor.moveToNext();
        }
        cursor.close();

        return list;
    }
}
