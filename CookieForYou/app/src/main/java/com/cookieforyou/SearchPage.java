package com.cookieforyou;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cookieforyou.model.Recipe;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import static com.cookieforyou.StartPage.selectedItem;

import java.util.ArrayList;
import java.util.List;

public class SearchPage extends AppCompatActivity implements OnQueryTextListener{

    List<Recipe> recipes = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public static ListView list;
    public static String searchText;
    ListViewAdapter adapter;
    SearchView editSearch;
    BottomNavigationView bottomNavigation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_page);

        bottomNavigation = findViewById(R.id.bottom_navigation);    //alsó menü navigáció view beállítása
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        //adatbázis beállítás
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.openDatabase();

        recipes = databaseAccess.getRecipes();                  //receptek lekérése az adatbázisból
        databaseAccess.closeDatabase();

        list = (ListView) findViewById(R.id.listview);          //lista view megtalálsa

        adapter = new ListViewAdapter(this, recipes);   //adapter beállítás

        // Adaptert összekapcsolja a list view-val
        list.setAdapter(adapter);
        list.setOnItemClickListener(onItemClickListener);

        //kereső mező, beállítása
        editSearch = (SearchView) findViewById(R.id.search);
        editSearch.setOnQueryTextListener(this);

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //openPage(SearchMorePage.class);     //ha lesz, Bővített keresés
                notWork("Ez a funkció még nem elérhető");
            }
        });
    }

    public void openPage(Class activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }

    //Találati istaelemre kattintás
    final ListView.OnItemClickListener onItemClickListener =
            new ListView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getBaseContext(), OneRecipePage.class);
                    //intent.putExtra("itemId", id + ""); //+ "" to make it string, of course you can use long but you have to remeber to use intent.getLongExtra(...) in details Activity
                    selectedItem = list.getItemIdAtPosition(position);
                    intent.putExtra("selectedItem",selectedItem);
                    startActivity(intent);
                }
            };

    //Alsó menu navigáció
    final BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {                                             //4 menű választása
                        case R.id.start:
                            Intent intent = new Intent(getBaseContext(), StartPage.class);  //kezdőoldal megnyitása
                            startActivity(intent);
                            return true;
                        case R.id.saved:
                            notWork("Ez a funkció még nem elérhető");                  //mentett receptek (nem működik)
                            return true;
                        case R.id.search:
                            Intent intent2 = new Intent(getBaseContext(), SearchPage.class);    //Keresés oldal megnyitása
                            startActivity(intent2);
                            return true;
                        case R.id.profil:
                            notWork("Ez a funkció még nem elérhető");                  //profil (nem működik)
                            return true;
                    }
                    return false;
                }
            };

    //Keresés küldése, enter/keresés gombal
    public boolean onQueryTextSubmit(String query) {
        Intent intent = new Intent(getBaseContext(), RecipesPage.class);    //megnyitja a talált receptek oldalt
        startActivity(intent);
        return false;
    }

    //Keresett szöveg írása alapján történő szűrés
    public boolean onQueryTextChange(String newText) {
        searchText = newText;
        adapter.filter(searchText);
        return false;
    }

    //Nem működő funkciókra
    public void notWork(String message){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}

